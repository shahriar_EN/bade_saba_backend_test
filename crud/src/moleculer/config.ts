import { BrokerOptions } from 'moleculer';

export function createConfig():BrokerOptions {
     const config: BrokerOptions = {
        transporter: process.env.TRANSPORTER,
      }
      return config
}