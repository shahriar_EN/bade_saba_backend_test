import { ExceptionFilter, Catch, ArgumentsHost, BadRequestException } from '@nestjs/common';
import { Response } from 'express';
import { IncomingMessage } from 'http';
import { HttpException, HttpStatus } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { ShahriarError } from './shahriar.error';

export const getStatusCode = (exception: unknown): number => {
  if(exception instanceof ShahriarError){
    return (exception as ShahriarError).status
  }else return exception instanceof HttpException
    ? exception.getStatus()
    : HttpStatus.INTERNAL_SERVER_ERROR;
};

export const getDetail = (exception: any): string => {
  let detail = ""
  if(exception instanceof ShahriarError){
    detail = (exception as ShahriarError).reason
  }else if(exception && exception.response ){
      if (typeof exception.response ==="string")
          detail = exception.response
      else if(typeof exception.response.message==="string")
          detail = exception.response.message
      else if  (exception.response.message && Array.isArray(exception.response.message)) {
        exception.response.message.forEach(res => {
          if(detail==="")
            detail =res
          else 
            detail +=`-${res}`
        })
      };
  }
  return detail
  
};

export const getErrorMessage = (exception: any): string => {
  if(exception instanceof ShahriarError){
    return (exception as ShahriarError).showMessage
  }else{
    return String(exception);
  }
  
};
export const getCode = (exception: any): number => {
  if(exception instanceof ShahriarError){
    return (exception as ShahriarError).code
  }else{
    return 0;
  }
  
};

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {

  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<IncomingMessage>();
    const httpStatus = getStatusCode(exception);
    const message = getErrorMessage(exception);
    const detail = getDetail(exception);
    const code = getCode(exception);
    // response.status(status).json({
    //   error: {
    //     timestamp: new Date().toISOString(),
    //     path: request.url,
    //     code,
    //     message
    //   },
    // });

    const { httpAdapter } = this.httpAdapterHost;


  

    const responseBody = {
      statusCode: httpStatus,
      timestamp: new Date().toISOString(),
      path: httpAdapter.getRequestUrl(ctx.getRequest()),
      message,
      detail,
      errorCode:code
    };
    httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
  }
}