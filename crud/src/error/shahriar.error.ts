import { HttpException, HttpStatus } from '@nestjs/common';
import List from './list.error.json';
export class ShahriarError {
    status: number;
    reason: string;
    showMessage:string;
    code:number;
    constructor(code: number, {language = 'ir',reason = ""}={language:"ir",reason:""}) {
    let error = List[code.toString()];
    if (!error) {
      error = List['1000'];
    }

    error.show = error[language];
    if (!error.show) error.show = List.defaultMessage[language];
    this.status = error.status || 500
    this.reason = reason || error.reason || "some thing gose wrong in error list",
    this.showMessage = error.show || "please call support team"
    this.code = code || 1
}
}
