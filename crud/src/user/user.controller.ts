import { Body, Controller, Get, Logger, Param, Post, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { MoleculerAdapter } from 'src/moleculer/adapter';
import { AuthDto, ProfileDto, Role } from './dto';
import { Auth, User } from './decorator';
import { IUser } from './interfaces/user.interface';

@Controller('users')
export class UserController {
  logger = new Logger('UserController');
  constructor(private readonly userService: UserService,private readonly moleculerAdapter:MoleculerAdapter) {}


  @Auth(Role.ADMIN)
  @Get()
  async getUsers() {
    return await this.userService.getUsers()
  }
  @Post()
  async auth(@Body() authDto:AuthDto){
      
       return this.userService.createUser(authDto)
  }


  @Post("/login")
  async signin(@Body() authDto:AuthDto){
    return this.userService.login(authDto)
  }

  @Auth()
  @Put("/me")
  async updateInfo(@Body() profileDto:ProfileDto,@User()user:IUser){
    return this.userService.updateInfo(profileDto,user._id)
  }

  @Auth(Role.ADMIN)
  @Put(":_id")
  async updateOtherUser(@Param() params,@Body() profileDto:ProfileDto){
    return this.userService.updateInfo(profileDto,params._id)
  }

  
}
 