import { IsNotEmpty } from "class-validator"

export class UpdateDto {
 
    
    @IsNotEmpty()
    firstname:string

    @IsNotEmpty()
    lastname:string
}