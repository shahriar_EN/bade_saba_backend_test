import {  IsNotEmpty } from "class-validator"

export class ProfileDto {
 
    @IsNotEmpty()
    firstname:string

    @IsNotEmpty()
    lastname:string
}