import { Expose } from 'class-transformer';
import { IsDefined, IsString } from 'class-validator';

export class HeadersDTO {
  @IsString()
  @IsDefined()
  @Expose({ name: 'authorization' })
  authorization: string; // note the param here is in double quotes
}
