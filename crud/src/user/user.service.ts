import { HttpException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { IUser } from './interfaces/user.interface';
import { exec } from 'child_process';
import { AuthDto } from './dto/auth.dto';
import { ShahriarError } from '../error/shahriar.error';
import * as argon from 'argon2';
import { SignUpRes, UpdateProfileRes, UserResDto } from './dto';
import { MoleculerAdapter } from 'src/moleculer/adapter';
import { ProfileDto } from './dto/profile.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<IUser>,
    private readonly moleculerAdapter: MoleculerAdapter,
  ) {}
  async createUser(authDto: AuthDto): Promise<SignUpRes> {
    const savedUser = await this.userModel.findOne({ email: authDto.email }).exec();
    if (savedUser) throw new ShahriarError(1001);
    const hash = await argon.hash(authDto.password);
    authDto.password = hash;
    const user = new this.userModel(authDto);
    await user.save();
    try{
        const token = await this.moleculerAdapter.call("users.generateToken",{_id:user._id.toString()})
        if(!token || typeof token !=="string")
            throw new Error("token dose not create correctly, token= "+token)
        return new SignUpRes(token,authDto.email);
    }catch(e){
        const reason  = e.message
        throw new ShahriarError(1002,{reason})
    } 
  }

  async login(authDto: AuthDto): Promise<SignUpRes> {
    const savedUser = await this.userModel.findOne({ email: authDto.email }).exec();
    if (!savedUser) throw new ShahriarError(1003);
    const pwMatches = await argon.verify(savedUser.password,authDto.password)
    if(!pwMatches) throw new ShahriarError(1004);
    try{
        const token = await this.moleculerAdapter.call("users.generateToken",{_id:savedUser._id.toString()})
        if(!token || typeof token !=="string")
            throw new Error("token dose not create correctly, token= "+token)
        return new SignUpRes(token,authDto.email);
    }catch(e){
        const reason  = e.message
        throw new ShahriarError(1002,{reason})
    } 
  }
  async updateInfo(profileDto:ProfileDto,id?:String):Promise<UpdateProfileRes>  {
    const user = await this.userModel.findById(id).exec();
    if(!user)
      throw new ShahriarError(1006);
    const updateUser = await this.userModel.findOneAndUpdate({_id:id},profileDto,{new:true}).exec();
    return new UpdateProfileRes(updateUser.firstname,updateUser.lastname)  
  }

  async getUsers():Promise<UserResDto[]> {
    const users = await this.userModel.find().exec();
    return users.map(user=>new UserResDto(user.firstname,user.lastname,user._id,user.email));
  }
}
