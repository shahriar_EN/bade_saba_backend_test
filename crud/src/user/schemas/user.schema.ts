import * as  mongoose from "mongoose"

export const UserSchema = new mongoose.Schema( {
    firstname: { type: String },
    lastname: { type: String },
    email: { type: String,unique: true  },
    password: { type: String },
    isAdmin:{type:Boolean,default:false}
});