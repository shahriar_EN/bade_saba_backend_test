import {
  CanActivate,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { ShahriarError } from '../../error/shahriar.error';
import { Role } from '../dto';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly role: Role) {}
  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (this.role == Role.ADMIN && !user.isAdmin) throw new ShahriarError(1007);
    return true;
  }
}
