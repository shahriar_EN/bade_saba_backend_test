import {
  CanActivate,
  createParamDecorator,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  ValidationError,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { HeadersDTO } from '../dto/bearer.dto';
import { ShahriarError } from '../../error/shahriar.error';
import { MoleculerAdapter } from 'src/moleculer/adapter';

@Injectable()
export class TokenGuard implements CanActivate {
    constructor(private readonly moleculerAdapter: MoleculerAdapter) {}
  async validateToken(dto: HeadersDTO,moleculerAdapter:MoleculerAdapter,request:any): Promise<boolean> {
    const errors: ValidationError[] = await validate(dto);
    if (errors.length > 0) {
      //Get the errors and push to custom array
      let validationErrors = errors.map((obj) =>
        Object.values(obj.constraints),
      );
      throw new ShahriarError(1002, { reason: validationErrors.toString() });
    }

    const user = await moleculerAdapter.call("users.validateToken",{token:dto.authorization})
    if(!user)
      throw new ShahriarError(1003)
    request.user = user
    return true;
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    
    // Convert headers to DTO object
    const dto = plainToInstance(HeadersDTO, headers, {
      excludeExtraneousValues: true,
    });

    return this.validateToken(dto,this.moleculerAdapter,request);

  }
}
