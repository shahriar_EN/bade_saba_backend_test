import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Role } from '../dto';
import { RoleGuard, TokenGuard, } from './index';
import { HeadersDTO } from '../dto';


export function Auth(role: Role=Role.USER) {
  return applyDecorators(
    // RequestHeader(HeadersDTO),
    UseGuards(TokenGuard,new RoleGuard(role)),
    // UseGuards(AuthGuard, RolesGuard),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
  );
}