import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    // ClientsModule.register([{
    //   name:"Auth",
    //   transport:Transport.NATS
    // }]),
    ConfigModule.forRoot({
      isGlobal:true
    }),
    MongooseModule.forRoot("mongodb://mongo/saba"),
    UserModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
