import { Logger, ValidationPipe } from '@nestjs/common';
import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { GlobalExceptionFilter } from './error/filter.error';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger('Main:bootstrap');
  const config = new DocumentBuilder()
    .setTitle('Saba')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('users')
    .build();
  

  const app = await NestFactory.create(AppModule);
  
  const httpAdapter = app.get(HttpAdapterHost);
  // const instance = httpAdapter.getInstance();

  app.useGlobalFilters(new GlobalExceptionFilter(httpAdapter));
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform:true
    }),
  );
  await app.listen(3000, () => {
    logger.log('start crud server on port 3000');
  });
}
bootstrap();
