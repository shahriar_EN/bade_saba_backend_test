"use strict";
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const DbMixin = require("../mixins/db.mixin");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "users",
	// version: 1
	
	/**
	 * Mixins
	 */
	mixins: [
		DbMixin("users", {
			firstname: { type: String },
			lastname: { type: String },
			email: { type: String },
			password: { type: String },
		}),
	],

	/**
	 * Settings
	 */
	settings: {},
	/**
	 * Action Hooks
	 */
	hooks: {},

	/**
	 * Actions
	 */
	actions: {
		token: {
			async handler({ params }) {
				console.log(params);

				return await this.adapter.findOne({ firstname: "shahriar" });
			},
		},

		validateToken: {
			/**
			 * validate token and return User or Error
			 * @param {Object} params
			 * @param {string} params.token
			 * @returns {(User|Error)}
			 */
			async handler({ params }) {
				console.log("token", params.token);
				const token = params.token.replace("Bearer ", "");
				const decodeToken = await this.deCodeToken(token);
				if (!decodeToken) return undefined;
				const user = await this.adapter.findById(decodeToken._id);
				if (!user) return undefined;
				return user;
			},
		},
		//call "users.generateToken" "{_id:"1"}"
		generateToken: {
			/**
			 * find user and generate a token for it
			 * @param {Object} params
			 * @param {string} params._id
			 * @returns {string} token
			 */
			async handler({ params }) {
				// const user = await this.adapter.findOne({email:"shahriarenayaty76@outlook.com"})
				const user = await this.adapter.findById(params._id);
				if (user) {
					const newToken = jwt.sign(
						{
							_id: user._id.toString(),
						},
						"shahriar",
						{
							expiresIn: "10h",
						}
					);
					return newToken;
				} else {
					return undefined;
				}
			},
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Loading sample data to the collection.
		 * It is called in the DB.mixin after the database
		 * connection establishing & the collection is empty.
		 */
		async seedDB() {
			await this.adapter.insertMany([
				{
					firstname: "shahriar",
					lastname: "enayaty",
					emaill: "shahriarenayaty76@outlook.com",
					password: "123456789",
				},
			]);
		},

		async deCodeToken(token) {
			return jwt.verify(token, "shahriar", function (error, decoded) {
				if (error && error.message === "jwt expired") {
					return undefined;
				} else if (error) {
					return undefined;
				} else {
					return decoded;
				}
			});
		},
	},

	/**
	 * Fired after database connection establishing.
	 */
	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	},
};
